# README #

This project is an extension designed to run on the Simple:Press Wordpress Theme Platform.  

### How do I get set up? ###

This theme is not a normal WordPress theme and cannot be installed via the WordPress THEMES screen.
Instead, you should:

- Download the theme file from your receipt or from your dashboard
- Go to FORUM->Themes
- click on `Theme Uploader`
- Click on the `Choose File` button and select the zip file you downloaded earlier
- Click the `Upload Now` button
- Go back to the FORUM->Themes screen and activate the theme.


### Change Log  ###
-----------------------------------------------------------------------------------------
###### 2.0.1
- Tweak: Use a better theme image

###### 2.0.0
- New: 6.0 compatibility

###### 1.0.0
- Initial release